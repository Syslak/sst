# Deprecated
No longer maintainded, as I have moved to a peronalized st build, [GO HERE!](https://gitlab.com/Syslak/st)

# SST - Syslak [ST (suckless terminal)](https://st.suckless.org/) Build

Fork of [Luke Smith's ST build](https://github.com/LukeSmithxyz/st) with some added patches.

## Bindings
+ **Scrollback** with `alt+k`, `alt+J` or `alt+d`, `alt+u` or `alt+pageup/down`
+ **Zoom** `crtl-plus`, `ctrl-minus`
+ **Copy** with `alt+c`
+ **Paste** with `alt+v` or `shift-insert`

## ST patches

+ Alpha focus
+ Fix keyboard Input
+ Scrollback
+ Vertcenter

## Features
+ Emoji support (requiers [libxft-bgra](https://aur.archlinux.org/packages/libxft-bgra/))
+ Powerline symbols

### Features from Luke Smith's build (using dmenu)

+ **Follow urls** with `alt+l`
+ **Copy urls** with `alt+y`
+ **Copy the output of commnads** with alt-o
